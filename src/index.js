import express from "express";
import connect from "./config/db/index.js";
import route from "./routes/index.js";
const app = express();
const port = 3000;

// connect to db
connect();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

route(app);
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});