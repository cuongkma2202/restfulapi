import express from "express";
import Book from "../app/Controllers/BookController.js";

const router = express.Router();

const BookController = new Book();

router.post("/store", BookController.store);
router.get("/:id", BookController.findOne);
router.delete("/:id", BookController.deleteBook);
router.put("/:id", BookController.updateBook);
router.patch("/:id", BookController.deleteBook);
router.get("/", BookController.show);
export default router;