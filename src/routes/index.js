import bookRouter from "./book.js";
const route = (app) => {
    app.use("/book", bookRouter);
};
export default route;