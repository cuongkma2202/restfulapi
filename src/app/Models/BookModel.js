import mongoose from "mongoose";
const Schema = mongoose.Schema;

const BookSchema = new Schema({
    name: { type: String, required: true },
    author: { type: String, required: true },
    category: { type: String, required: true },
    price: { type: Number, required: true },
}, {
    timestamps: true,
});
const Book = mongoose.model("Book", BookSchema);
export default Book;