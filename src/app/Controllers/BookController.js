import Book from "../Models/BookModel.js";

class BookController {
    //  GET ALL [GET] /book
    show(req, res) {
            Book.find({})
                .then((book) => {
                    res.status(200).json({
                        status_code: 200,
                        message: "Successfully",
                        data: book,
                    });
                })
                .catch((err) => {
                    res.status(500).json({
                        status_code: 500,
                        message: err.message,
                    });
                });
        }
        // Create book [POST] /book/store
    store(req, res) {
            const fromData = {...req.body };
            console.log(req.body);
            const newBook = new Book(fromData);
            // console.log(newBook);
            newBook
                .save()
                .then((book) => {
                    res.status(200).json({
                        status_code: 200,
                        message: "New book added",
                        data: book,
                    });
                })
                .catch((err) => {
                    res.status(500).json({
                        status_code: 500,
                        message: err.message,
                    });
                });
        }
        // Find one book [GET] /book/:id
    async findOne(req, res) {
            try {
                const itemBook = await Book.findById(req.params.id);
                if (itemBook) {
                    res.status(200).json({
                        status_code: 200,
                        message: "Find Successfully",
                        data: itemBook,
                    });
                } else {
                    res.status(404).json({
                        status_code: 404,
                        message: "Cannot find book",
                    });
                }
            } catch (err) {
                res.status(500).json({
                    status_code: 500,
                    message: err.message,
                });
            }
        }
        // DELETE one book [DELETE] /book/:id
    async deleteBook(req, res) {
            try {
                const itemDel = await Book.findByIdAndDelete({ _id: req.params.id });
                if (itemDel) {
                    res.status(200).json({
                        status_code: 200,
                        message: "Delete Successfully",
                    });
                } else {
                    res.status(404).json({
                        status_code: 404,
                        message: "Cannot find book",
                    });
                }
            } catch (err) {
                res.status(500).json({
                    status_code: 500,
                    message: err.message,
                });
            }
        }
        // edit book [PUT] : /book/:id + formData
    async updateBook(req, res) {
        const formData = {...req.body };
        try {
            const itemUpdate = await Book.findOneAndReplace({ _id: req.params.id },
                formData
            );
            if (itemUpdate) {
                res.status(200).json({
                    status_code: 200,
                    message: "Update Successfully",
                    data: itemUpdate,
                });
            } else {
                res.status(404).json({
                    status_code: 404,
                    message: "Cannot find book",
                });
            }
        } catch (err) {
            res.status(500).json({
                status_code: 500,
                message: err.message,
            });
        }
    }

    // edit patch [PATCH]/ formData
    async editBook(req, res) {
        const formData = {...req.body };
        try {
            const itemUpdate = await Book.findOneAndUpdate({ _id: req.params.id },
                formData
            );
            if (itemUpdate) {
                res.status(200).json({
                    status_code: 200,
                    message: "Update Successfully",
                    data: itemUpdate,
                });
            } else {
                res.status(404).json({
                    status_code: 404,
                    message: "Cannot find book",
                });
            }
        } catch (err) {
            res.status(500).json({
                status_code: 500,
                message: err.message,
            });
        }
    }
}

export default BookController;