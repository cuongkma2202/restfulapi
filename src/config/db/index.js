import mongoose from "mongoose";

const connect = async() => {
    try {
        await mongoose.connect("mongodb://localhost:27017/db_test_dev", {});
        console.log("connect to mongoDB successfully");
    } catch (err) {
        console.log(err);
    }
};
export default connect;